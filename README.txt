Diceware Dutch
==============

Generates randomly a requested amount of Dutch words. The words are
distilled from the listings at OpenTaal, and only contain words longer
than 3 characters, but shorter than 9.

Cf. https://en.wikipedia.org/wiki/Diceware


Usage
-----

* Request four Dutch words:

    ./dice.sh 4

* Request seven Dutch words:

    ./dice.sh 7


Dependencies
------------

This script just depends on bash, the GNU coreutils and /dev/urandom.
