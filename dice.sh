#!/bin/bash


# Find out what directory our script is located at.

    SCRIPT_DIR=`dirname $(readlink -f $0)`;
    WORD_FILE=$SCRIPT_DIR/wordlist;


# A short usage reminder

    dice_usage() {
        echo "Please use an integer as the first and only parameter.";
        echo "Example usage to obtain seven random words:";
        echo "";
        echo "dice.sh 7";
        echo "";
        exit;
    }


# A simple validation of our input

    case $1 in
        ''|*[!0-9]*) dice_usage;;
    esac


# Find out if the word file is reachable, and shuf is installed.

    which shuf > /dev/null || { echo "Error: Please install shuf (coreutils)."; exit; }
    test -e $WORD_FILE || { echo "Error: Could not find wordlist."; exit; };


# Let's go.

    WORDS_AVAILABLE=`cat $SCRIPT_DIR/wordlist | wc -l`;
    WORDS_REQUESTED=$1;
    COUNTER=0

    while [ $COUNTER != $WORDS_REQUESTED ]; do
        sed -n -e `shuf \
            -i 1-"$WORDS_AVAILABLE" -n 1 \
            --random-source=/dev/urandom`p \
            $SCRIPT_DIR/wordlist;
        COUNTER=$[$COUNTER +1]
    done
